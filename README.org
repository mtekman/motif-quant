#+TITLE: A write up of the Motif Analysis

* Detecting Motifs

*** Workflow

   Motifs were generated using my workflow developed in Galaxy, published under the name:
   
   =Motif Analysis of Two Sets of Peaks with Homer (Enriching one on the other)=

   [[https://usegalaxy.eu/u/mehmet-tekman/w/motif-analysis-of-two-sets-of-peaks-with-homer-enriching-one-on-the-other][Link to workflow in Galaxy]]

   [[./galaxy_workflow.png]]


   
*** Output Histories

  The workflow takes two peaks lists as inputs, and so for this we took the whole peaks
  file from MAC2, as well as the narrow peaks from MACS2 and performed the following
  comparisons between the Eomes-unique, Bra-unique, and common peaks MACS2 lists, with a
  window of +/- 75bp to help the overlap.

  The rows are the Treatment peaks fed into Homer, and the columns are their accompanying Background peaks.

**** Narrow Peaks
  

  | Treat\Backg        | (780) Eomes-unique          | (995) Bra-unique             | (826) Common                 |
  |--------------------+-----------------------------+------------------------------+------------------------------|
  | (780) Eomes-unique |                             | 780_bg-995_eo-on-bra.zip     | 780_bg-826_eo-on-common.zip  |
  | (995) Bra-unique   | 995_bg-780_bra-on-eo.zip    |                              | 995_bg-826_bra-on-common.zip |
  | (826) Common       | 826_bg-780_common-on-eo.zip | 826_bg-995_common-on-bra.zip |                              |

**** Whole Peaks list

 |                    | (727) Eomes-unique                    | (726) Bra-unique                       | (818) Common                           |
 |--------------------+---------------------------------------+----------------------------------------+----------------------------------------|
 | (727) Eomes-unique |                                       | 727_bg-726_eowhole-on-brawhole.zip     | 727_bg-818_eowhole-on-commonwhole.zip  |
 | (726) Bra-unique   | 726_bg-727_brawhole-on-eowhole.zip    |                                        | 726_bg-818_brawhole-on-commonwhole.zip |
 | (818) Common       | 818_bg-727_commonwhole-on-eowhole.zip | 818_bg-726_commonwhole-on-brawhole.zip |                                        |

** Discovered Motifs

*** Eomes

    [[images/eomes_top_motifs.png]]

**** Eomes1: GTGWBA
    
   [[images/eomes_denovo_1_GTGWBA.png]]

**** Eomes2: GATWA

   [[images/eomes_denovo_2_GATWA.png]]

**** Eomes3: GHAAAYA

   [[images/eomes_denovo_3_GHAAAYA.png]]

*** Brachyury

  [[images/brachyury_top_motifs.png]]

**** Brachyury1
  
   [[images/brachyury_denovo_1.png]]

**** Brachyury2

   [[images/brachyury_denovo_2.png]]

*** Common
**** Common1

    [[images/common_denovo_1.png]]

 
* Counting Motifs

*** FIMO

  To count the occurrence of the motifs discovered in the previous step, we initially
  considered using the FIMO tool as part of the MEME suite, but we ran into technical
  issues relating to the Eomes motif being too small to be detected (according to FIMO).

  An occurrence is counted as valid if the motif binds to a region with a p-value
  threshold smaller than 1e-4 (by default). For the Brachyury and Common motifs, their
  occurrences were able to be counted both across the genome as well as in peak regions
  using this default p-value threshold.

  However, Eomes had some issues due to how small it is. The program kept giving me 0
  occurrences in both the genome and the peak regions because the p-value was always too
  large.
  
  Despite repeated attempts involving lowering the threshold of detectability (p-value <
  0.0025), no threshold could be determined that could be used across the other motifs
  (Brachyury and Common) to give sensible results.

  For posterity the notebook that attempted to make sense of these results is linked [[file:fimo_motifs_summarized.org][here]],
  the output datasets [[file:datasets/fimo_output_datasets.tar.gz][here]], and the script to collect and process the output of FIMO is
  included below:

  #+begin_src R
    library(tidyverse)

    ## unpack above datasets
    dname <- "/galaxy_results/"
    datasets <- list.files(dname)

    processFile <- function(dataset) {
      tab <- as_tibble(read.table(paste0(dname, dataset), sep = "\t"))
      tab <- tab %>%  separate(col = V9, into = c("name", "alias", "id", "pval", "qval", "sequence", "junk"), sep = ";")
      ## Remove junk from columns
      tab$V1 <- gsub("\\(\\.\\)", "", tab$V1)
      tab$chrom <- gsub("^(chr[^:]+):(.+)$", "\\1", tab$V1)
      tab$name <- gsub("Name=", "", tab$name)
      tab$alias <- gsub("Alias=", "", tab$alias)
      tab$id <- gsub("ID=", "", tab$id)
      tab$pval <- as.numeric(gsub("pvalue=", "", tab$pval))
      tab$qval <- as.numeric(gsub("qvalue= ", "", tab$qval))
      tab$sequence <- gsub("sequence=", "", tab$sequence)
      tab$seqcapital <- toupper(tab$sequence)
      tab <- tab %>% select(pos = V1, chrom= chrom, seqcapital = seqcapital, pval = pval, qval = qval, strand = V7)
      return(tab)
    }

    processed <- sapply(datasets, processFile, simplify=F, USE.NAMES = T)

    computeStats <- function(datasetname){
      proctable <- processed[[datasetname]]
      ## collapse unique positions
      num_matches <- nrow(proctable)
      uniq_positions <- unique(sort(proctable$pos))
      num_uniq_positions <- length(uniq_positions)
      chrom_freqs <- table(gsub("^(chr[^:]+):(.+)$", "\\1", uniq_positions))
      tmp <- table(proctable$seqcapital)
      num_seq_frequencies <- length(tmp)
      seq_frequencies_gt1 <- sort(tmp[tmp > 1], decreasing = T)
      seq_frequencies_top10 <- head(seq_frequencies_gt1, 10)
      num_seq_frequencies_gt1 <- length(seq_frequencies_gt1)
      title <- gsub("^(.*)peaks_on_(.*)\\.gff", "\\U\\2 motif in \\1 peaks", datasetname, perl=T)

      return(list(title = title,
                  n.pos = num_matches,
                  n.uniq.pos = num_uniq_positions,
                  chrom.freqs = chrom_freqs,
                  seq.freqs.top10 = seq_frequencies_top10,
                  n.seq.freqs = num_seq_frequencies,
                  n.seq.freqs.g1 = num_seq_frequencies_gt1))
    }

    processed.stats <-  sapply(datasets, computeStats, simplify = F, USE.NAMES = T)

    printStats <- function(datasetname){
      options(width=400)
      proctable <- processed.stats[[datasetname]]
      cat(paste0("** ", proctable$title, " with p-value < 1e-4\n"))
      cat("\n*** Position Info\n")
      cat(paste0(" - Number of Matches = ", proctable$n.pos, "\n"))
      cat(paste0(" - Number of Matches with Unique Positions = ", proctable$n.uniq.pos, "\n"))
      cat("\n*** Chromosome Distribution\n")
      cat("starttable")
      print(t(t(proctable$chrom.freqs[paste0("chr", c(c(1:19),"X","Y"))])))
      cat("endtable")
      cat("\n*** Top 10 Sequences and Frequencies:\n")
      cat(paste0(" - Number of Unique Sequences = ", proctable$n.seq.freqs, "\n"))
      cat(paste0("           of which occur > 1 = ", proctable$n.seq.freqs.g1))
      cat("starttable")
      print(t(t(proctable$seq.freqs.top10)))
      cat("endtable")
      cat("\n\n\n")
    }

    zzz <- capture.output(res = lapply(names(processed.stats), printStats))
    fileconn <- file(paste0(dname, "fimo_results_summarized.org"))
    writeLines(zzz, fileconn)
    close(fileconn)

    print(paste0(dname, "fimo_results_summarized.org"))
  #+end_src

  
  
  
*** BioStrings

    To circumvent the small motif limitations, we switched to the R library [[https://bioconductor.org/packages/release/bioc/html/Biostrings.html][BioStrings]] and
    extracted probabilistic matches with a threshold over 85% on any reading frame.

    The notebook of results included [[file:biostrings_results.org][here]], and the full notebook of scripts to generate it
    is included [[file:biostrings_analysis.org][here]], and the input PWM's and peaks are given [[file:datasets/input_memepwm_and_peakfasta.tar.gz][here]].

    The main steps and scripts are summarized below
    
** Calculate Occurences at each Peak position and for each Chromosome
    
    #+begin_src R
      library(tidyverse)
      library(Biostrings)
      library(BSgenome.Mmusculus.UCSC.mm10)
      library(universalmotif)

      chroms = paste0("chr", c(c(1:19),"X","Y"))
      genome = Mmusculus

      dname = "biostrings/"
      outdir = "biostrings/results_indiv_nodupes/"
      dir.create(outdir)

                                              # Read in Data
      ## unpack input_memepwm_and_peakfasta.tar.gz
      motifs.list = list.files(paste0(dname, "motifs"))
      peaks.list = list.files(paste0(dname, "peaks"))

      unique.motifs = unique(sort(gsub("(.*)\\..*", "\\1", motifs.list)))
      unique.peaks = gsub("peaks\\.(.*)\\.75bp\\.fasta", "\\1", peaks.list)

      readUniqueDNAStringSet <- function(peakfile){
        peak <- readDNAStringSet(peakfile)
        peak <- peak[unique(names(peak))]
        return(peak)
      }

      read_peak <- function(peakname){
        peakfile <- paste0(dname, "peaks/peaks.", peakname, ".75bp.fasta")
        peak <- readUniqueDNAStringSet(peakfile)
        ##allpeaks <- do.call(xscat,peak) ## combine all peaks into one string
        ##return(allpeaks)
        return(peak)
      }

      read_motif <- function(motifname){
        cmatrix = t(read.table(paste0(dname, "motifs/", motifname, ".count"),
                               col.names=c("A","C","G","T")))
        bkg = read_meme(paste0(dname, "motifs/", motifname,".meme"))@bkg
        motif = PWM(x=cmatrix, prior.params=bkg)
        return(motif)
      }

                                              # Process Data
      motifs = sapply(unique.motifs, read_motif, simplify=F, USE.NAMES=T)
      peaks = sapply(unique.peaks, read_peak, simplify=F, USE.NAMES=T)
      score.thresh = "85%"

      run_motif_against_chrom <- function(motifname, chromname){
        mot <- motifs[[motifname]]
        chr <- genome[[chromname]]
        res <- matchPWM(mot, chr, with.score=T, min.score=score.thresh)
        scores <- mcols(res)
        together <- cbind(as.data.frame(res), scores)

        outname = paste0(outdir, "motif-", motifname, "_mm10-", chromname, ".tsv")
        write.table(together, file=outname, quote=F, sep="\t", col.names=NA)
      }



      run_motif_against_peakgroup <- function(motifname, peakname){
        mot <- motifs[[motifname]]
        allpeaknames <- names(peaks[[peakname]])

        hits_per_peak <- sapply(allpeaknames, function (pk.name){
          pk.string <- peaks[[peakname]][[pk.name]]
          res <- matchPWM(mot, pk.string, with.score=T, min.score=score.thresh)
          sco <- mcols(res)
          together <- cbind(as.data.frame(res), sco)
          ## e.g. "eouni_chr1_3399945-3400095_eomes"
          outname <- paste0(outdir, "/", peakname, "_", sub("\\(\\.\\)$", "", sub(":", "_", pk.name)),
                            "_", motifname, ".tsv")
          ##print(outname)
          write.table(together, file=outname, quote=F, sep="\t", col.names=NA)
          hits <- length(res)
          return(hits)
        }, simplify = T, USE.NAMES=T)
        return(as_tibble(cbind(names(hits_per_peak), hits_per_peak)))
      }

      convert_peakname_to_coords <- function(tibb, motifname, peakgroupname){
        tibb %>%
          separate(sep=":", col=V1, into=c("chrom", "pos")) %>%
          separate(sep="\\(", col=pos, into=c("pos", "junk")) %>%
          separate(sep="-", col=pos, into=c("start", "end")) %>%
          mutate(start=as.integer(start), end=as.integer(end), motif=motifname, peak=peakgroupname) %>%
          select(chrom,start,end, hits=hits_per_peak, peak, motif) %>%
          arrange(chrom, start, end)
      }


      all_hits <- tibble(chrom=character(0), start=integer(0), end=integer(0), hits=integer(0), peak=character(0))
      for (motif in unique.motifs){
        for (peak in unique.peaks){
          print(paste("Running:", motif, "on", peak))
          coo <- convert_peakname_to_coords(run_motif_against_peakgroup(motif, peak), motif, peak)
          all_hits <- rbind(all_hits, coo)
        }
        for (chrom in chroms){
          print(paste("Running:", motif, "on", chrom))
          run_motif_against_chrom(motif, chrom)
        }
      }
      all_hits$hits = as.integer(all_hits$hits)
      write.table(all_hits, file=paste0(outdir, "../all_hits.tsv"), quote=F, sep="\t", col.names=NA)

      ## Any duplicates preventing us from converting this into a matrix?
      ## all_hits %>% arrange(chrom, start, end, peak, motif) %>% add_count(chrom,start,end,peak,motif) %>% filter(n>5)
      ## Yes plenty -- for example "chr8  123554262 123554412" is called 6 times in the common peaks file, this is because
      ## the file literally defines this region 6 times!

      ## This means we can deduplicate easily, and the number of hits should be the same.
      hit_matrix <-all_hits %>% arrange(chrom, start, end, hits, peak, motif) %>% distinct() %>% spread(motif, hits)
      write.table(hit_matrix, file=paste0(outdir, "../hit_matrix.tsv"),
                  quote=F, sep="\t", col.names=NA)

    #+end_src

    In the case of the peak positions, this generates a massive 21,368 x 10 feature matrix
    of individual peaks as rows and individual motifs as columns.

    A sample of this can be seen below:

|   | chrom |   start |     end | peak   | brachyury1 | brachyury2 | common1 | eomes1 | eomes2 | eomes3 |
|---+-------+---------+---------+--------+------------+------------+---------+--------+--------+--------|
| 1 | chr1  | 3191769 | 3191919 | common |          1 |          0 |       1 |      2 |      1 |      1 |
| 2 | chr1  | 3399945 | 3400095 | eouni  |          0 |          0 |       0 |      2 |      0 |      0 |
| 3 | chr1  | 3671757 | 3671907 | common |          0 |          0 |       0 |      0 |      0 |      0 |
| 4 | chr1  | 3994826 | 3994976 | eouni  |          0 |          0 |       0 |      0 |      0 |      0 |
    
    In a given peak, a motif can occur more than once and so these occurrences are counted for in each peak.
    The full "hit_matrix" can be found [[file:datasets/hit_matrix.tsv.tar.gz][here]].

** Counting the occurences

    To count the number occurrences of a given motif in a given peak, we collapse the above rows:

    #+begin_src R
      library(Biostrings)
      library(tidyverse)

      mat <- "hit_matrix.tsv"
      hit_matrix <- read_tsv(mat) %>% mutate(chrom=chrom,
               start=as.integer(start), end=as.integer(end),
               peak=peak,
               brachyury1=as.integer(brachyury1),
               brachyury2=as.integer(brachyury2),
               common1=as.integer(common1),
               eomes1=as.integer(eomes1), eomes2=as.integer(eomes2),
               eomes3=as.integer(eomes3))

      ## Verifying that the common motif binds 196 times to the common peaks?
      ## > nrow(hit_matrix %>% filter(peak=="common", common1 > 0))
      ## 195 -- so we had one duplicate before, which makes sense.

                                              # Generating Motif Bed files
      outdir = "biostrings/bedfiles/"
      peaks_dir = "biostrings/peaks/"
      dir.create(outdir)

      bed_from_motif <- function(motifname){
        hit_matrix %>%
          filter(get(motifname) > 0) %>%
          mutate(name=paste0(motifname, "_", peak)) %>%
          select(chrom,start,end,name) %>%
          write_tsv(paste0(outdir, "/", motifname, ".bed"), col_names=F)
      }

      bed_from_motif_inpeak <- function(motifname, inpeak){
        hit_matrix %>%
          filter(get(motifname) > 0, peak==inpeak) %>%
          mutate(name=paste0(motifname, "_", peak)) %>%
          select(chrom,start,end,name) %>%
          write_tsv(paste0(outdir, "/", motifname, "_in_peak-", inpeak, ".bed"), col_names=F)
      }

      bed_from_peaks <- function(peakname){
        strings <- readUniqueDNAStringSet(paste0(peaks_dir, "/peaks.", peakname, ".75bp.fasta"))
        as_tibble(names(strings)) %>% separate(value, into=c("chrom", "name"), sep="\\(\\.") %>%
          separate(chrom, into=c("chrom", "start", "end"), sep="[:-]") %>%
          mutate(start=as.integer(start), end=as.integer(end), name=paste0("Peak_",peakname)) %>%
          arrange(chrom, start, end) %>%
          write_tsv(paste0(outdir, "/", "peak_", peakname, ".bed"), col_names=F)
      }


      multiplemotifs_in_peak <- function(motifname, peakname){
        tmp_num_peaks = Inf
        filt_gt = 1
        box_data <- tibble(filt=integer(0), npeaks=integer(0))
        logdir <- paste0(outdir, "filterlogs")
        dir.create(logdir, showWarnings=F)
        ## 
        while(tmp_num_peaks > 0){
          pks <- hit_matrix %>% filter(peak==peakname, get(motifname) > filt_gt) %>%
            select("chrom", "start", "end", "peak", motifname)
          pks %>% write_tsv(paste0(logdir, "/", motifname, "_in_peak-", peakname, "_occurrences_gt_",
                                   sprintf("%02d", filt_gt), ".bed"),
                            col_names=F)
          tmp_num_peaks <- nrow(pks)
          box_data <- box_data %>% add_row(filt=filt_gt, npeaks = tmp_num_peaks)
          filt_gt <- filt_gt + 1
        }

        nice_peakname <- function(peakname){
          list("eouni" = "Eomes-Unique",
               "brauni" = "Brachyury-Unique",
               "common" = "Common")[[peakname]]
        }
        p1 <- box_data %>% ggplot(aes(x=filt, y=npeaks)) + geom_col() +
          xlab("X times") + ylab("Number of Peaks (Log10 scale)") +
          scale_x_continuous(breaks=c(min(box_data$filt):max(box_data$filt))) +
          scale_y_log10(breaks=c(c(1:5), seq(6,10,2),seq(10,50,10),
                                 seq(60,100,20), seq(100,500,100),
                                 seq(600,1000, 200),
                                 seq(1000,10000,1000))) +
          ggtitle(sprintf("Number of peaks that are bound by the %s motif\nmore than X times in the %s Peaks",
                          motifname, nice_peakname(peakname)))
        ggsave(paste0(logdir, "/", motifname, "_in_peak-", peakname, "_occurences_gt.png"),
               p1,
               height=1600, width=1600, units="px")
      }


      bed_from_motif("brachyury1")
      ##bed_from_motif("brachyury2")
      bed_from_motif("common1")
      bed_from_motif("eomes1")
      ##bed_from_motif("eomes2")
      ##bed_from_motif("eomes3")

      bed_from_motif_inpeak("brachyury1", "brauni")
      bed_from_motif_inpeak("brachyury1", "eouni") 
      ##bed_from_motif_inpeak("brachyury2", "brauni")
      bed_from_motif_inpeak("common1", "common")
      bed_from_motif_inpeak("eomes1", "eouni")
      bed_from_motif_inpeak("eomes1", "brauni")
      ## bed_from_motif_inpeak("eomes2", "eouni")
      ## bed_from_motif_inpeak("eomes3", "eouni")


      bed_from_peaks("common")
      bed_from_peaks("eouni")
      bed_from_peaks("brauni")

      multiplemotifs_in_peak("brachyury1", "brauni")
      multiplemotifs_in_peak("brachyury1", "eouni")
      multiplemotifs_in_peak("common1", "common")
      multiplemotifs_in_peak("eomes1", "eouni") 
      multiplemotifs_in_peak("eomes2", "eouni") 
      
    #+end_src

        
** Find differential binding sites between motifs for common peaks

   We load the hit matrix and look for peak sites where the number of motifs between
   conditions are differentially expressed (i.e. significantly more occurrences of one
   motif compared to another at the same peak position).

  #+begin_src R
    library(tidyverse)
    library(rGREAT)

    mat <- "hit_matrix.tsv"
    hit_matrix <- read_tsv(mat) %>% mutate(chrom=chrom,
             start=as.integer(start), end=as.integer(end),
             peak=peak,
             brachyury1=as.integer(brachyury1),
             brachyury2=as.integer(brachyury2),
             common1=as.integer(common1),
             eomes1=as.integer(eomes1), eomes2=as.integer(eomes2),
             eomes3=as.integer(eomes3)) %>% select(-brachyury2,-eomes2,-eomes3,-common1)


    ## Create an annotated matrix from GREAT
    nrow(hit_matrix) ## 21, 368 regions
    ## bed <- hit_matrix %>% select(chrom, start, end)
    ## job <- submitGreatJob(bed, rule = "twoClosest", adv_twoDistance=2000, species="mm10")


    out.dir = "~/regions/"
    dir.create(out.dir)

    annotateRegions <-function(regions){
      job <- submitGreatJob(regions, species="mm10")
      ## maximum of 500 regions in tsv
      tb <- getEnrichmentTables(job, download_by = "tsv")
      rr <- as_tibble(
        tb[["GO Molecular Function"]][,c("Regions","Genes")]
      ) %>% distinct()
      rr %>%
        ## dedup regions with more than one gene
        filter(!(str_detect(Genes, pattern=".*,.*"))) %>%
        ## Group by regions which 
        group_by(Regions) %>%
        ## paste genes that share the same regions together..
        summarize(Genes=paste0(Genes, collapse=",")) %>%
        separate(Regions, sep="[:-]",
                 into=c("chrom", "start", "end")) %>%
        mutate(start=as.integer(start), end=as.integer(end)) %>%
        arrange(chrom, start, end)
    }


                                            # Eo with Eomes
    writeTables.eomes <- function(bragt = 0, bramult = 1){
      peakname <- "eouni"
      tab <- hit_matrix %>%
        filter(peak==peakname, brachyury1 > bragt) %>%
        filter(eomes1 > (bramult * brachyury1)) %>%
        mutate(name=paste0(peak, "_brachyury1=",
                           brachyury1, "_eomes1=", eomes1)) %>%
        select(chrom,start,end,name)
      tab.name <- paste0(out.dir,
                         peakname,
                         "_bra-gt-", bragt,
                         "_and_bra-gt-eo-by-", bramult,
                         "-times.n=", nrow(tab), ".bed")
      get.genes <- annotateRegions(tab)
      res <- merge(tab, get.genes, by=c("chrom", "start", "end"))
      res %>% write_tsv(tab.name, col_names=FALSE)
      return(res)
    }


    ## 347 Eo-unique regions where Bra binds at least once, but Eomes more than Bra
    writeTables.eomes(0, 1)

    ## 114 Eo-unique regions where Bra binds at least once, but Eomes at least twice more than Bra
    writeTables.eomes(0, 2)

    ## 36 Eo-unique regions where Bra binds at least once, but Eomes at least thrice more than Bra
    writeTables.eomes(0, 3)

    ## 17 Eo-unique regions where Bra binds at least once, but Eomes at least 4 times more than Bra
    writeTables.eomes(0, 4)

    ## 22 Eo-unique regions where Bra binds at least twice, but Eomes binds more:
    writeTables.eomes(1, 1)

    ## 12 Eo-unique regions where Bra binds at least twice, but Eomes binds more
    writeTables.eomes(2, 1)

                                            # Bra with Brachyury
    writeTables.bra <- function(eogt = 0, eomult = 1){
      peakname <- "brauni"
      tab <- hit_matrix %>%
        filter(peak==peakname, eomes1 > eogt) %>%
        filter(brachyury1 > (eomult * eomes1)) %>%
        mutate(name=paste0(peak, "_eomes1=",
                           eomes1, "_brachyury1=", brachyury1)) %>%
        select(chrom,start,end,name)
      tab.name <- paste0(out.dir,
                         peakname,
                         "_eo-gt-", eogt,
                         "_and_eo-gt-bra-by-", eomult,
                         "-times.n=", nrow(tab), ".bed")
      get.genes <- annotateRegions(tab)
      res <- merge(tab, get.genes, by=c("chrom", "start", "end"))
      res %>% write_tsv(tab.name, col_names=FALSE)
      return(res)
    }

    ## 110 Bra-unique regions where Eomes binds at least once, but Bra more
    writeTables.bra(0, 1)

    ## 5 Bra-unique regions where Eomes binds at least once, but Bra twice as much
    writeTables.bra(0, 2)

    ## 12 Bra-unique regions where Eomes binds at least twice, but Bra more
    writeTables.bra(1, 1)
  #+end_src  
  
    